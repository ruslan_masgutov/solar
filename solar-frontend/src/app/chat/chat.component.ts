import {Component, OnInit, OnDestroy} from "@angular/core";
import {WebSocketService} from "../service/websocket.service";
import {Http} from "@angular/http";

@Component({
    selector: 'solar-chat',
    template: require('./chat.component.html').toString()
})
export class ChatComponent implements OnInit, OnDestroy {
    users: any[];
    currentMessage: string;
    messages: any[];

    constructor(private http: Http, private websocketService: WebSocketService) {
    }

    sendMessage(event: any) {
        event.preventDefault();
        this.websocketService.sendMessage({
            sender: JSON.parse(localStorage.getItem('user-info')),
            message: this.currentMessage
        });
        this.currentMessage = '';
    }

    ngOnInit(): void {
        let onNewUser = message => {
            let user = JSON.parse(message.body);
            this.users.push(user);
        };
        let onNewMessage = message => {
            let data = JSON.parse(message.body);
            this.messages.push(data);
            setTimeout(ChatComponent.scrollDown(), 1000);
        };

        this.http.get('/chat').subscribe((response) => {
            this.users = response.json()
        });
        this.http.get('/chat/messages').subscribe((response) => {
            this.messages = response.json()
        });
        this.websocketService.connectUser(onNewUser);
        this.websocketService.connectMessage(onNewMessage);
        ChatComponent.scrollDown();
    }


    ngOnDestroy(): void {
        this.websocketService.disconnect();
    }

    static scrollDown(){
        let output = document.getElementById('chat-output');
        output.scrollTop = output.scrollHeight;
    }

}