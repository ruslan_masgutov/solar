import {Injectable} from "@angular/core";

let SockJS = require("sockjs-client");
let StompWrapper = require("stompjs/lib/stomp.js");

@Injectable()
export class WebSocketService {
    private stompClientUser: any;
    private stompClientMessage: any;

    sendMessage(message: any) {
        this.stompClientMessage.send('/app/send', {}, JSON.stringify(message))
    }

    connectUser(onMessage?: (message: any)=>void,): void {
        let defaultOnMessage = message => {
            console.log('Message: ' + message);
            console.log('Message body: ' + message.body);
        };
        onMessage = onMessage || defaultOnMessage;

        let socket = new SockJS('http://localhost:9020/websocket-point');
        this.stompClientUser = StompWrapper.Stomp.over(socket);
        this.stompClientUser.connect({}, frame => {
            console.log('Connected: ' + frame);
            this.stompClientUser.subscribe(`/topic/users`, onMessage);
        })
    }

    connectMessage(onMessage?: (message: any)=>void,): void {
        let defaultOnMessage = message => {
            console.log('Message: ' + message);
            console.log('Message body: ' + message.body);
        };
        onMessage = onMessage || defaultOnMessage;

        let socket = new SockJS('http://localhost:9020/websocket-point');
        this.stompClientMessage = StompWrapper.Stomp.over(socket);
        this.stompClientMessage.connect({}, frame => {
            console.log('Connected: ' + frame);
            this.stompClientMessage.subscribe(`/topic/messages`, onMessage);
        })
    }

    disconnect(): void {
        this.stompClientUser.disconnect();
        this.stompClientMessage.disconnect();
        console.log("Disconnected");
    }

}