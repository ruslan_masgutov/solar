import {Component} from "@angular/core";

require('./app.less');

@Component({
    selector: 'solar-app',
    template: `<div class="container">
    <router-outlet></router-outlet>
</div>
`,
})
export class AppComponent {
}
