import {Component} from "@angular/core";
import {Http} from "@angular/http";
import {Router} from "@angular/router";

@Component({
    selector: 'solar-user-entry',
    template: require('./user-entry.component.html').toString()
})
export class UserEntryComponent {
    name: string;
    color: string;

    constructor(private http: Http, private router: Router) {
    }

    join(event) {
        event.preventDefault();
        let errorCallback = (error) => {
            alert(error.text());
            console.log(error.text());
        };
        let data = {
            name: this.name,
            color: this.color
        };
        localStorage.setItem('user-info', JSON.stringify(data));
        this.http.post('/chat/join', data).subscribe(() => {
            this.router.navigate(['/chat']);
        }, errorCallback);

    }
}