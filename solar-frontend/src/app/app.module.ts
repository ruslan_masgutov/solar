import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import {routing, appRouterProviders} from "./app.routes";
import {AppComponent} from "./app.component";
import {ChatComponent} from "./chat/chat.component";
import {UserEntryComponent} from "./user-entry/user-entry.component";
import {FormsModule} from "@angular/forms";
import {WebSocketService} from "./service/websocket.service";

@NgModule({
    imports: [BrowserModule, HttpModule, routing, FormsModule],
    declarations: [AppComponent, ChatComponent, UserEntryComponent],
    bootstrap: [AppComponent],
    providers: [appRouterProviders, WebSocketService]
})
export class AppModule {
}