import { ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import {UserEntryComponent} from "./user-entry/user-entry.component";
import {ChatComponent} from "./chat/chat.component";

export const APP_ROUTES: Routes = [
    {path: '', component: UserEntryComponent, pathMatch: 'full'},
    {path: 'chat', component: ChatComponent, pathMatch: 'full'},
];

export const appRouterProviders = [];

export const routing:ModuleWithProviders = RouterModule.forRoot(APP_ROUTES, {useHash: true});

