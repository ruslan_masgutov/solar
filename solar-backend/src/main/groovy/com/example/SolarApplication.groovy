package com.example

import groovy.transform.CompileStatic
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@CompileStatic
@SpringBootApplication
class SolarApplication {

    static void main(String[] args) {
        SpringApplication.run(SolarApplication.class, args)
    }
}
