package com.example.service

import com.example.dto.Message
import com.example.dto.User
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@CompileStatic
@Service
class ChatService {
    List<User> users = Collections.synchronizedList([])
    List<Message> messageHistory = Collections.synchronizedList([])
    @Autowired
    SimpMessagingTemplate simpMessagingTemplate

    void joinToChat(User user) {
        users.add(user)
        simpMessagingTemplate.convertAndSend('/topic/users', user)
    }

}
