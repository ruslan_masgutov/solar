package com.example.controller

import com.example.dto.Message
import com.example.dto.User
import com.example.service.ChatService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Controller
@RequestMapping(value = '/chat')
class ChatController {
    @Autowired
    ChatService chatService

    @GetMapping
    @ResponseBody
    List<User> getUsers() {
        chatService.users
    }

    @GetMapping(value = '/messages')
    @ResponseBody
    List<Message> getMessages() {
        chatService.messageHistory
    }

    @MessageMapping('/send')
    @SendTo('/topic/messages')
    Message sendMessage(Message message) {
        chatService.messageHistory.add(message)
        message
    }

    @PostMapping('/join')
    @ResponseBody
    void joinToChat(@RequestBody User user) {
        chatService.joinToChat(user)
    }
}
