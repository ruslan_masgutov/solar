package com.example.dto

import groovy.transform.CompileStatic
import groovy.transform.ToString

@CompileStatic
@ToString
class User {
    String name
    String color
}
