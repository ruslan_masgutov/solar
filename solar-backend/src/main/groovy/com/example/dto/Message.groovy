package com.example.dto

import groovy.transform.CompileStatic
import groovy.transform.ToString

@CompileStatic
@ToString
class Message {
    User sender
    String message
}
