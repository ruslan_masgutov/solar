package com.example.configuration

import groovy.transform.CompileStatic
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Configuration
@EnableWebSocketMessageBroker
class WebSocketSecurityConfiguration extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint('/websocket-point').setAllowedOrigins('*').withSockJS()
    }

    @Override
    void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker('/queue')
        registry.enableSimpleBroker('/topic')
        registry.setApplicationDestinationPrefixes('/app')
    }

}
